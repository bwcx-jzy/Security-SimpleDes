 simple 简单的des 加密工具 8位key
 
 使用方法：
    
    SystemKey systemKey = new SystemKey("test");
    String mima = systemKey.encrypt("test123");
    System.out.println("加密后：" + mima);
    System.out.println("解密：" + systemKey.decrypt(mima));
   
如果key 小于8位自动补零，大于8位只取前8位