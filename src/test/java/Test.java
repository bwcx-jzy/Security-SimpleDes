import cn.jiangzeyin.des.SystemKey;

/**
 * Created by jiangzeyin on 2018/9/19.
 */
public class Test {
    public static void main(String[] args) throws Exception {
        SystemKey systemKey = new SystemKey("test");
        String mima = systemKey.encrypt("test123");
        System.out.println("加密后：" + mima);
        System.out.println("解密：" + systemKey.decrypt(mima));
    }
}
